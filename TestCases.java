package AvenueCodeTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import AvenueCodeTests.MyTasksObjects;
import org.junit.Assert;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestCases {
	public WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() {
		  System.setProperty("webdriver.chrome.driver", "E:\\Selenium Practice\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.get("http://qa-test.avenuecode.com");
		  driver.manage().window().maximize();
		  
	  }
	
	@Test (priority=1)
  public void login( ) {
		MyTasksObjects.signIn(driver).click();
		MyTasksObjects.username(driver).sendKeys("priyanka.t.88@gmail.com");
		MyTasksObjects.password(driver).sendKeys("priyanka");
		MyTasksObjects.login(driver).click();
  }
	
	@Test(priority=2, dependsOnMethods={"login"})
	  public void createTask( ) {
		//Create a new task
		String taskDesc="Testing a new task";
			MyTasksObjects.myTaskLink(driver).click();
			MyTasksObjects.newTask(driver).sendKeys(taskDesc);
			MyTasksObjects.createTaskButton(driver).click();
			
			//Create task by hitting enter
			MyTasksObjects.newTask(driver).sendKeys(taskDesc);
			MyTasksObjects.newTask(driver).sendKeys(Keys.ENTER);
			Assert.assertEquals(MyTasksObjects.newTaskCreated(driver).getText(),taskDesc);
	  }
	
	@Test(priority=2, dependsOnMethods={"createTask"})
	  public void createSubTask( ) {
		
		//Get current date
		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy ");
		 Date d = new Date();
		 String date= dateFormat.format(d);
		 
		 //Create Subtask
		MyTasksObjects.manageSubtask(driver).click();
		MyTasksObjects.subtaskDescription(driver).sendKeys("New SubTask");
		MyTasksObjects.subtaskDueDate(driver).clear();
		MyTasksObjects.subtaskDueDate(driver).sendKeys(date);
		MyTasksObjects.submitSubtask(driver).click();
		Assert.assertEquals("New SubTask", MyTasksObjects.subtaskCreated(driver).getText());
		MyTasksObjects.closeSubtask(driver).click();
			
	  }
	
	@Test(priority=2, dependsOnMethods={"editTask"})
	  public void editSubTask( ) {
		 
		MyTasksObjects.manageSubtask(driver).click();
		
		//Check for error when subtask description is blank
		MyTasksObjects.subtaskCreated(driver).click();
		MyTasksObjects.editSubtaskDesc(driver).clear();
		MyTasksObjects.editSubtaskDesc(driver).sendKeys(Keys.ENTER);
		Assert.assertEquals("Task can't be blank!",MyTasksObjects.blankSubTaskError(driver).getText() );
		
		//Enter valid subtaask description and save
		MyTasksObjects.editSubtaskDesc(driver).sendKeys("Edited Subtask");
		MyTasksObjects.editSubtaskDesc(driver).sendKeys(Keys.ENTER);
		MyTasksObjects.closeSubtask(driver).click();
			
	  }
	
	@Test(priority=2, dependsOnMethods={"createSubTask"})
	  public void checkSubtaskCount( ) {
		 
			String e=MyTasksObjects.manageSubtask(driver).getText();
		     Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(e);
		     String number="";
		     while(m.find()) {
		    	 number+=(m.group(1)); 
			 }
		     Assert.assertEquals("1", number);
	  }
	
	@Test(priority=2, dependsOnMethods={"createTask"})
	  public void editTask( ) {
		 
			MyTasksObjects.newTaskCreated(driver).click();
			
			//Check for error when Task description is blank
			MyTasksObjects.editTask(driver).clear();
			MyTasksObjects.editTask(driver).sendKeys(Keys.ENTER);
			Assert.assertEquals("Task can't be blank!", MyTasksObjects.blankTaskError(driver).getText());
			
			//Enter valid task description and save
			MyTasksObjects.editTask(driver).sendKeys("Edited task");
			MyTasksObjects.editTask(driver).sendKeys(Keys.ENTER);
	  }
	
	
	 @AfterTest
	  public void afterTest() {
		 //logout and exit
//		 MyTasksObjects.logout(driver).click();
//		 driver.quit();
	  }

}
