package AvenueCodeTests;


import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.javascript.host.Console;

public class MyTasksObjects {
	
	 private static WebElement element = null;
	 
	 public static WebElement signIn (WebDriver driver){
		 
         element = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a"));
 
         return element;
 
         }
	 
 public static WebElement username (WebDriver driver){
		 
         element = driver.findElement(By.id("user_email"));
 
         return element;
 
         }
 
 public static WebElement password (WebDriver driver){
	 
     element = driver.findElement(By.id("user_password"));

     return element;

     }
 
public static WebElement login (WebDriver driver){
	 
     element = driver.findElement(By.xpath("//*[@id=\"new_user\"]/input"));
     return element;

     }

public static WebElement myTaskLink (WebDriver driver){
	 
    element = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a"));

    return element;

    }

public static WebElement newTask (WebDriver driver){
	
	WebElement element = null;
	 
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//*[@id=\"new_task\"]"))));
	 
	return element;

}

public static WebElement createTaskButton(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[2]/span"))));
	 
	return element;

}
public static WebElement newTaskCreated(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[2]/a"))));
	 
	return element;
}

public static WebElement editTask(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[2]/form/div/input"))));
	 
	return element;
}

public static WebElement blankTaskError(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[2]/form/div/div"))));
	 
	return element;
}

public static WebElement blankSubTaskError(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/form/div/div"))));
	 
	return element;
}

public static WebElement subtaskCreated(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/a"))));
	 
	return element;
}

public static WebElement manageSubtask(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[4]/button"))));
	return element;
}
 
public static WebElement subtaskDescription(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//*[@id=\"new_sub_task\"]"))));
	return element;
}


public static WebElement subtaskDueDate(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//*[@id=\"dueDate\"]"))));
	return element;
}

public static WebElement editSubtaskDesc(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/form/div/input"))));
	return element;
}
public static WebElement submitSubtask(WebDriver driver){
	
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//*[@id=\"add-subtask\"]"))));
	return element;
}

public static WebElement closeSubtask(WebDriver driver){
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[4]/div/div/div[3]/button"))));
	return element;
}

public static WebElement deleteTask(WebDriver driver){
	WebDriverWait wait = new WebDriverWait(driver, 20);
	element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/button"))));
	return element;
}
public static WebElement logout (WebDriver driver){
	 
    element = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[2]/a"));

    return element;

    }
}
